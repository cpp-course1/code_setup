# Code_setup

First git project for the course. Will be used to test how to set up different projects etc.


# Compilation
###  Use cmake to create make file 
### and then compile with the make command
### The CMakeLists.txt file was created using 
#### https://medium.com/heuristics/c-application-development-part-3-cmakelists-txt-from-scratch-7678253e5e24
mkdir build   
cd build    
cmake ..   
make    


# Run
cd build
./main


# code structure
root          
  | ---- CMakeLists.txt             
  | ---- .gitignore   
  | ---- README.md   
  |   
  ---- src     
  |     | ---- main.cpp    
  |       
  ---- include   
  |     | ----- compute    
  |             | ----- operators.h     
  |   
  ---- tests     
  |   
  ---- libs    
